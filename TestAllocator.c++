// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator
#include <sstream>
#include <string>

#include "gtest/gtest.h"

#include "Allocator.h"
#include "Solve.h"

// --------------
// TestAllocator1
// --------------

template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>
    my_types_1;

TYPED_TEST_CASE(TestAllocator1, my_types_1);

TYPED_TEST(TestAllocator1, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, test_2) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// --------------
// TestAllocator2
// --------------

template <typename A>
struct TestAllocator2 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
my_allocator<int,    100>,
             my_allocator<double, 100>>
             my_types_2;

TYPED_TEST_CASE(TestAllocator2, my_types_2);

TYPED_TEST(TestAllocator2, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator2, test_2) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TYPED_TEST(TestAllocator2, test_3) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test

TYPED_TEST(TestAllocator2, test_4) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    const allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test

TEST (AllocatorFixture, read_int_1) {
    std::string s = "2";
    ASSERT_EQ(read_int(s), 2);
}

TEST (AllocatorFixture, read_int_2) {
    std::string s = "-2";
    ASSERT_EQ(read_int(s), -2);
}

TEST (AllocatorFixture, allocator_print_1) {
    my_allocator<double, 1000> alloc;
    alloc.allocate(5);
    alloc.allocate(10);
    std::ostringstream w;
    allocator_print(alloc, w);
    ASSERT_EQ(w.str(), "-40 -80 856\n");
}

TEST (AllocatorFixture, allocator_print_2) {
    my_allocator<double, 1000> alloc;
    double* b = alloc.allocate(5);
    alloc.allocate(7);
    alloc.allocate(3);
    alloc.deallocate(b, 5);
    std::ostringstream w;
    allocator_print(alloc, w);
    ASSERT_EQ(w.str(), "40 -56 -24 848\n");
}

TEST (AllocatorFixture, allocator_deallocate_1) {
    my_allocator<double, 1000> alloc;
    alloc.allocate(6);
    alloc.allocate(3);
    alloc.allocate(8);
    allocator_deallocate(alloc, -2);

    my_allocator<double, 1000> alloc2;
    alloc2.allocate(6);
    double* p = alloc2.allocate(3);
    alloc2.allocate(8);
    alloc2.deallocate(p, 3);

    my_allocator<double, 1000>::iterator i = std::begin(alloc);
    ++i;
    my_allocator<double, 1000>::iterator i2 = std::begin(alloc2);
    ++i2;
    ASSERT_EQ(*i, *i2);
}

TEST (AllocatorFixture, allocator_deallocate_2) {
    my_allocator<double, 1000> alloc;
    alloc.allocate(6);
    alloc.allocate(3);
    alloc.allocate(5);
    allocator_deallocate(alloc, -2);
    allocator_deallocate(alloc, -1);

    my_allocator<double, 1000> alloc2;
    double* p = alloc2.allocate(6);
    double* p2 = alloc2.allocate(3);
    alloc2.allocate(5);
    alloc2.deallocate(p, 6);
    alloc2.deallocate(p2, 3);

    my_allocator<double, 1000>::iterator i = std::begin(alloc);
    my_allocator<double, 1000>::iterator i2 = std::begin(alloc2);
    ASSERT_EQ(*i, *i2);
}

TEST (AllocatorFixture, allocator_solve_1) {
    std::istringstream r("1\n\n5\n3\n-1");
    std::ostringstream w;
    allocator_solve(r, w);
    ASSERT_EQ(w.str(), "40 -24 912\n");
}

TEST (AllocatorFixture, allocator_solve_2) {
    std::istringstream r("1\n\n5\n3\n1\n-1\n-1");
    std::ostringstream w;
    allocator_solve(r, w);
    ASSERT_EQ(w.str(), "72 -8 896\n");
}

TEST (AllocatorFixture, allocator_solve_3) {
    std::istringstream r("2\n\n2\n5\n1\n6\n8\n-2\n-2\n-2\n\n2\n5\n1\n6\n8\n-4\n-3\n-2");
    std::ostringstream w;
    allocator_solve(r, w);
    ASSERT_EQ(w.str(), "-16 112 -64 776\n-16 112 -64 776\n");
}

TEST (AllocatorFixture, allocator_solve_4) {
    std::istringstream r("2\n\n2\n7\n3\n-2\n6\n\n2\n7\n3\n-2\n2");
    std::ostringstream w;
    allocator_solve(r, w);
    ASSERT_EQ(w.str(), "-16 -56 -24 872\n-16 -16 32 -24 872\n");
}