// --------------
// RunAllocator.c++
// --------------

// Template taken from RunCollatz.c++

// --------
// includes
// --------

#include <iostream> // cin, cout
#include "Solve.h"

// ----
// main
// ----

int main () {
    using namespace std;
    allocator_solve(cin, cout);
    return 0;
}

