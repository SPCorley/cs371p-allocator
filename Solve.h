#ifndef Solve_h
#define Solve_h

#include <string>
#include <iostream>
#include "Allocator.h"

int read_int (std::string&);
void allocator_print (const my_allocator<double, 1000>, std::ostream&);
void allocator_deallocate (my_allocator<double, 1000>&, int);
void allocator_solve (std::istream&, std::ostream&);
#endif
