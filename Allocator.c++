#include <cassert>
#include <iostream>
#include <sstream>
#include <string>
#include "Solve.h"

using namespace std;

#define N 1000

/*
 * Takes in string and reads and returns an
 * int from the string
 */
int read_int (string& s) {
    assert(s.length() != 0);
    istringstream sin(s);
    int x;
    sin >> x;
    return x;
}

/*
 * Takes in an allocator and prints each sentinel to the ostream
 */
void allocator_print (const my_allocator<double, N> alloc, ostream& w) {
    my_allocator<double, N>::const_iterator b = begin(alloc);
    my_allocator<double, N>::const_iterator e = end(alloc);
    w << *b;
    ++b;
    while (b != e) {
        w << " " << *b;
        ++b;
    }
    w << "\n";
}

/*
 * Takes in an allocator and a index based at -1 and
 * deallocates the used block corresponding to the index
 */
void allocator_deallocate (my_allocator<double, N>& alloc, int input) {
    my_allocator<double, N>::iterator i = begin(alloc);
    while (input != -1) {
        while (*i > 0)
            ++i;
        ++i;
        ++input;
    }
    while (*i > 0)
        ++i;
    double* pointer = reinterpret_cast<double*>(&*i + 1);
    alloc.deallocate(pointer, abs(*i) / sizeof(double));
}

/*
 * Takes in the input and output streams and reads the input while
 * making the appropriate changes to the allocator and printing
 * the sentienls for each test
 */
void allocator_solve (istream& r, ostream& w) {
    int tests;
    string s;
    getline(r, s);
    tests = read_int(s);
    getline(r, s);
    for (int i = 0; i < tests; ++i) {
        int input;
        my_allocator<double, N> alloc;
        while (getline(r, s) && s.length() != 0) {
            input = read_int(s);
            if (input > 0)
                alloc.allocate(input);
            else if (input < 0)
                allocator_deallocate(alloc, input);
        }
        allocator_print(alloc, w);
    }
}
