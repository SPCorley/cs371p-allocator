// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <string>
#include <iostream>

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
    * O(1) in space
    * O(n) in time
    * Uses the iterators to check that each pair of sentinels
    * are equal and have the correct amount of space between them
    * and that everything adds up to N.
    */
    bool valid () const {
        const_iterator b = std::begin(*this);
        const_iterator e = std::end(*this);
        int total = 0;
        while (b != e) {
            total += abs(*b) + (2 * sizeof(int));
            const int *p = &*b;
            if (*p != *(p + (abs(*p) / sizeof(int)) + 1))
                return false;
            ++b;
        }
        return total == N;
    }


public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            _p += (abs(*_p) / sizeof(int)) + 2;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            _p -= (abs(*(_p - 1)) / sizeof(int)) + 2;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            _p += (abs(*_p) / sizeof(int)) + 2;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            _p -= (abs(*(_p - 1)) / sizeof(int)) + 2;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        if (N < sizeof(T) + (2 * sizeof(int)))
            throw std::bad_alloc();
        (*this)[0] = N - (2 * sizeof(int));
        (*this)[N - sizeof(int)] = N - (2 * sizeof(int));
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        iterator b = std::begin(*this);
        iterator e = std::end(*this);
        int min_bytes = n * sizeof(value_type);
        while (*b < min_bytes && b != e)
            ++b;
        if (b == e)
            throw std::bad_alloc();
        unsigned int free = *b - min_bytes;
        // If no bytes left over or not enough for a free block
        // then just change the sentinels to negative
        if (free == 0 || free < sizeof(value_type) + (2 * sizeof(int))) {
            int *s1 = &*b;
            int *s2 = s1 + (*s1 / sizeof(int)) + 1;
            *s1 -= 2 * *s1;
            *s2 = *s1;
            // Otherwise change the value of the sentinels and add
            // new ones for the free block
        } else {
            int size = n * sizeof(value_type) * -1;
            int new_size = free - (2 * sizeof(int));
            int *s = &*b;
            *s = size;
            s += (abs(size) / sizeof(int)) + 1;
            *s = size;
            ++s;
            *s = new_size;
            s += (new_size / sizeof(int)) + 1;
            *s = new_size;
        }
        assert(valid());
        int *p = &*b;
        return reinterpret_cast<pointer>(++p);
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     * <your documentation>
     */
    void deallocate (pointer p, size_type n) {
        int* b = reinterpret_cast<int*>(&a[0]);
        int* e = reinterpret_cast<int*>(&a[N - 1] + 1);
        int* s = reinterpret_cast<int*>(p);
        --s;
        int size = abs(*s);

        //Switch the sentinels' values to positive
        *s += size * 2;
        s += (*s / sizeof(int)) + 1;
        *s += size * 2;

        ++s;
        //If there's another free block after this one,
        //then coalesce them
        if (s < e && *s > 0) {
            int size_b2 = *s;
            s += (*s / sizeof(int)) + 1;
            *s += size + (2 * sizeof(int));
            s -= (*s / sizeof(int)) + 1;
            *s += size_b2 + (2 * sizeof(int));
            size = *s;
            s += (*s / sizeof(int)) + 2;
        }
        s -= (*(s - 1) / sizeof(int)) + 3;
        //If there's another free block before this one,
        //then coalesce them
        if (s > b && *s > 0) {
            int size_b2 = *s;
            s -= (*s / sizeof(int)) + 1;
            *s += size + (2 * sizeof(int));
            s += (*s / sizeof(int)) + 1;
            *s += size_b2 + (2 * sizeof(int));
            size = *s;
        }
        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(reinterpret_cast<int*>(&a[0]));
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(reinterpret_cast<const int*>(&a[0]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(reinterpret_cast<int*>(&a[N-1] + 1));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(reinterpret_cast<const int*>(&a[N-1] + 1));
    }
};

#endif // Allocator_h
